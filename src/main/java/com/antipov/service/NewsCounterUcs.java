package com.antipov.service;

import com.antipov.service.newscounter.NewsCounterService;
import com.antipov.service.pageloader.WebPageDownloadService;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class NewsCounterUcs {

    private final WebPageDownloadService pageDownloadService;
    private final NewsCounterService newsCounterService;
    private final NewsPrintService printService;

    public NewsCounterUcs(final WebPageDownloadService pageDownloadService,
                          final NewsCounterService newsCounterService,
                          final NewsPrintService printService) {
        this.pageDownloadService = pageDownloadService;
        this.newsCounterService = newsCounterService;
        this.printService = printService;
    }

    public void run() {
        try {
            printService.printNews(newsCounterService.extractCountOfArticles(pageDownloadService.download()));
        } catch (IOException e) {
            System.out.println("Sorry, service currently is unavailable");
        }
    }

}

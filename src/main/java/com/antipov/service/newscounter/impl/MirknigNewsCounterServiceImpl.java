package com.antipov.service.newscounter.impl;

import com.antipov.service.newscounter.AbstractNewsCounterService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

@Service
@Profile("mirknig")
public class MirknigNewsCounterServiceImpl extends AbstractNewsCounterService {

    @Override
    public long extractCountOfArticles(final String content){
        final String decodedRegex = decodeRegexFromIsoToUtf();
        final Pattern currentDayPattern = Pattern.compile(decodedRegex);
        return currentDayPattern.matcher(content).results().count();
    }

    private String decodeRegexFromIsoToUtf() {
        return new String(regexPattern.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    }
}

package com.antipov.service.newscounter.impl;

import com.antipov.service.newscounter.AbstractNewsCounterService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

@Service
@Profile("opennet")
public class OpennetNewsCounterServiceImpl extends AbstractNewsCounterService {

    @Override
    public long extractCountOfArticles(final String content){
        final String currentDate =  new SimpleDateFormat("dd.MM.yyyy").format(new Date());
        final Pattern currentDayPattern = Pattern.compile(String.format(regexPattern, currentDate));
        return currentDayPattern.matcher(content).results().count();
    }
}

package com.antipov.service.newscounter;


public interface NewsCounterService {
    long extractCountOfArticles(final String content);
}

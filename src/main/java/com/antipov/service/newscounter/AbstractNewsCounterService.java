package com.antipov.service.newscounter;

import org.springframework.beans.factory.annotation.Value;

public abstract class AbstractNewsCounterService implements NewsCounterService {

    @Value("${parser.regexp}")
    protected String regexPattern;
}

package com.antipov.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class NewsPrintService {

    @Value("${parser.name}")
    private String parserName;

    public void printNews(final long newsAmount) {
        System.out.printf("%s:%d%n", parserName, newsAmount);
    }
}

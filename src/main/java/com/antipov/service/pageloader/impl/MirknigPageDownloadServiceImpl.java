package com.antipov.service.pageloader.impl;

import com.antipov.service.pageloader.AbstractWebPageDownloadService;
import org.apache.http.client.fluent.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Profile("mirknig")
public class MirknigPageDownloadServiceImpl extends AbstractWebPageDownloadService {

    @Value("${parser.userAgent}")
    private String userAgent;

    @Override
    public String download() throws IOException {
        return Request.Get(pageUrl)
                .userAgent(userAgent)
                .execute()
                .returnContent()
                .asString();
    }
}

package com.antipov.service.pageloader.impl;

import com.antipov.service.pageloader.AbstractWebPageDownloadService;
import org.apache.http.client.fluent.Request;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Profile("opennet")
public class OpennetPageDownloadServiceImpl extends AbstractWebPageDownloadService {

    @Override
    public String download() throws IOException {
        return Request.Get(pageUrl)
                .execute()
                .returnContent()
                .asString();
    }
}

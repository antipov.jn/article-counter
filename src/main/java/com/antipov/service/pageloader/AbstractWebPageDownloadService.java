package com.antipov.service.pageloader;


import org.springframework.beans.factory.annotation.Value;

public abstract class AbstractWebPageDownloadService  implements WebPageDownloadService{

    @Value("${parser.url}")
    protected String pageUrl;
}

package com.antipov.service.pageloader;

import java.io.IOException;

public interface WebPageDownloadService {

    String download() throws IOException;
}
